<?xml version="1.0"?>
<recipe>
    <#include "../common/recipe_manifest.xml.ftl" />

    <#include "root/src/app_package/layout/simple_activity_layout.xml.ftl" />
    <open file="${escapeXmlAttribute(resOut)}/layout/${layoutName}.xml" />

	

    <instantiate from="root/src/app_package/ui/SimpleScope.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${scopeClass}.java" />

    <instantiate from="root/src/app_package/ui/SimpleActivity.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${activityClass}.java" />

    <open file="${escapeXmlAttribute(srcOut)}/${activityClass}.java" />
	
	
	
	<instantiate from="root/src/app_package/ui/SimpleFragment.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${fragmentClass}.java" />

    <open file="${escapeXmlAttribute(srcOut)}/${fragmentClass}.java" />
	
	
	
	<instantiate from="root/src/app_package/ui/SimplePresenter.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${presenterClass}.java" />

    <open file="${escapeXmlAttribute(srcOut)}/${presenterClass}.java" />
	
	
	
	
	<instantiate from="root/src/app_package/ui/SimpleContract.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${contractClass}.java" />

    <open file="${escapeXmlAttribute(srcOut)}/${contractClass}.java" />
	
	
	
	
    <instantiate from="root/src/app_package/ui/SimpleComponent.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${componentClass}.java" />

    <open file="${escapeXmlAttribute(srcOut)}/${componentClass}.java" />

<#if generateModule>
    <instantiate from="root/src/app_package/ui/SimpleModule.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${moduleClass}.java" />

    <open file="${escapeXmlAttribute(srcOut)}/${moduleClass}.java" />
</#if>


</recipe>
