package ${packageName};

import com.ruangguru.livestudents.ui.BaseFragment;
import static com.google.common.base.Preconditions.checkNotNull;

import com.ruangguru.livestudents.R;
import android.util.Log;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;

import javax.inject.Inject;

@EFragment(R.layout.fragment_)
public class ${fragmentClass} extends BaseFragment implements ${contractClass}.View {

    private static final String TAG = ${fragmentClass}.class.getSimpleName();
	
	@Inject ${presenterClass} mPresenter;
	
	@Override
    public void setPresenter(${contractClass}.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }
	
	@Override
    public void showError(Boolean isShown, String errorMessage, String errorLogMessage) {
        if (isShown) {
            showToast(errorMessage);
        }

        Log.e(TAG, errorLogMessage);
    }

    @UiThread
    @Override
    public void setLoadingIndicator(String message, boolean isActive) {
        if (isActive){
            showProgressDialog(message);
        } else {
            hideProgressDialog();
        }
    }
}