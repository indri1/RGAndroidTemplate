package ${packageName};

import com.ruangguru.livestudents.BasePresenter;
import com.ruangguru.livestudents.BaseView;


public interface ${contractClass}{
    
    interface View extends BaseView<Presenter> {

    }

    interface Presenter extends BasePresenter {

    }
}