package ${packageName};

import dagger.Module;
import dagger.Provides;

@Module
public class ${moduleClass} {
	private final ${contractClass}.View mView;

    public ${moduleClass}(${contractClass}.View mView) {
        this.mView = mView;
    }

    @Provides
    @${scopeClass}
    ${contractClass}.View provideView() {
        return mView;
    }
	
	
}
