package ${packageName};
 
import com.ruangguru.livestudents.BasePresenter;
import com.ruangguru.livestudents.BaseView;

public class ${presenterClass} implements ${contractClass}.Presenter{
    
	@Override
    public void subscribe() {
        
    }

    @Override
    public void unsubscribe() {

    }
}