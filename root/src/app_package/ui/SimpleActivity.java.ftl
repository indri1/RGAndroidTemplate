package ${packageName};

import ${superClassFqcn};

import android.os.Bundle;

import android.support.annotation.Nullable;
import org.androidannotations.annotations.EActivity;
import com.ruangguru.livestudents.R;
import com.ruangguru.livestudents.FragmentHostActivity;

import javax.inject.Inject;

@EActivity(R.layout.fragment_host)
public class ${activityClass} extends FragmentHostActivity {

    @Inject ${presenterClass} mPresenter;
	
	@Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState == null) initFragment();
        super.onCreate(savedInstanceState);
        initPresenter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (currentFragment == null) initFragment();
        if (mPresenter == null) initPresenter();
    }

	private void initFragment() {
        currentFragment = ${fragmentClass}_.builder().build();
    }
	
	private void initPresenter() {
	Dagger${componentClass}
                .builder()
                
                .${moduleClass?uncap_first}(new ${moduleClass}((${contractClass}.View)currentFragment))
				.build()
                .inject(this);
				
				
    }
}

