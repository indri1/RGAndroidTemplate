package ${packageName};

import dagger.Component;
import com.ruangguru.livestudents.data.RepositoryComponent;

@${scopeClass}
@Component( dependencies = ${parentComponentClass}.class <#if generateModule>, modules = ${moduleClass}.class </#if>)
public interface ${componentClass}{
    void inject(${activityClass} activity);
}